import React, { useState } from 'react';
import Link from 'next/link';
import constants from '~/public/static/data/my-constants/Constants';
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import LazyLoad from 'react-lazyload';
import { Col, Row, Space } from 'antd';

function BigCategories({ categories }) {
    const [itemCount, setItemCount] = useState(null);

    let bannerView;

    const img = new Image();
    img.onload = function () {
        if (this.width < 400) {
            setItemCount(2);
        } else {
            setItemCount(1);
        }
    };
    img.src =
        categories.length !== 0 &&
        `https://media.wired.com/photos/5d09594a62bcb0c9752779d9/1:1/w_1500,h_1500,c_limit/Transpo_G70_TA-518126.jpg`;

    if (itemCount !== null) {
        const responsive = {
            mobile: {
                breakpoint: { max: 576, min: 0 },
                items: itemCount,
            },
        };

        bannerView = (
            <Carousel
                swipeable={true}
                draggable={true}
                responsive={responsive}
                ssr={true}
                removeArrowOnDeviceType={['mobile']}
                itemClass="product-carousel__items">
                {categories.length !== 0 &&
                    categories.map((item, index) => {
                        return (
                            <Col  style={{ boxShadow: " rgba(0, 0, 0, 0.24) 0px 3px 8px", marginTop: "1rem", flex:1, }}>
                            <h4 style={{ textAlign: "center",paddingTop:'5px' }}>TV and Electronics</h4>
                            <Space
                                direction="horizontal"
                                style={{ width: '100%', justifyContent: 'space-around'}}>
                                <div>
                                    <img
                                        src="/static/img/slider/home-7/test10.jpeg"
                                        alt=""
                                        style={{ width: '100px', height: '100px' }}
                                    />
                                    <p>LG</p>
                                </div>

                                <div>
                                    <img
                                        src="/static/img/slider/home-7/test9.webp"
                                        alt=""
                                        style={{ width: '100px', height: '100px' }}
                                    />
                                    <p>Sony</p>
                                </div>
                            </Space>
                            <Space
                                direction="horizontal"
                                style={{ width: '100%', justifyContent: 'space-around' }}>
                                <div>
                                    <img
                                        src="/static/img/slider/home-7/test11.png"
                                        alt=""
                                        style={{ width: '100px', height: '100px' }}
                                    />
                                    <p>Asus </p>
                                </div>

                                <div>
                                    <img
                                        src="/static/img/slider/home-7/test12.jpg"
                                        alt=""
                                        style={{ width: '100px', height: '100px' }}
                                    />
                                    <p>DEll </p>
                                </div>
                            </Space>
                        </Col>
                        );
                    })}
            </Carousel>
        );
    }

    return (
        itemCount !== null && (
            <>
                <div className="banner-cards" >
                    {categories.length !== 0 &&
                        categories.map((item, index) => {
                            return (
                                <div
                                    style={{ padding: '1rem', paddingTop: "0", paddingBottom: "0", flex: 1, height: "25vw",  }}>


                                        <Col  style={{ boxShadow: " rgba(0, 0, 0, 0.24) 0px 3px 8px", marginTop: "1rem", flex:1, }}>
                                            <h4 style={{ textAlign: "center",paddingTop:'5px' }}>TV and Electronics</h4>
                                            <Space
                                                direction="horizontal"
                                                style={{ width: '100%', justifyContent: 'space-around'}}>
                                                <div>
                                                    <img
                                                        src="/static/img/slider/home-7/test10.jpeg"
                                                        alt=""
                                                        style={{ width: '100px', height: '100px' }}
                                                    />
                                                    <p>LG</p>
                                                </div>

                                                <div>
                                                    <img
                                                        src="/static/img/slider/home-7/test9.webp"
                                                        alt=""
                                                        style={{ width: '100px', height: '100px' }}
                                                    />
                                                    <p>Sony</p>
                                                </div>
                                            </Space>
                                            <Space
                                                direction="horizontal"
                                                style={{ width: '100%', justifyContent: 'space-around' }}>
                                                <div>
                                                    <img
                                                        src="/static/img/slider/home-7/test11.png"
                                                        alt=""
                                                        style={{ width: '100px', height: '100px' }}
                                                    />
                                                    <p>Asus </p>
                                                </div>

                                                <div>
                                                    <img
                                                        src="/static/img/slider/home-7/test12.jpg"
                                                        alt=""
                                                        style={{ width: '100px', height: '100px' }}
                                                    />
                                                    <p>DEll </p>
                                                </div>
                                            </Space>
                                        </Col>
                                      

                                </div>
                            );
                        })}
                </div>

                <div className="banner-card-mobile">{bannerView}</div>
            </>
        )
    );
}

export default BigCategories;
