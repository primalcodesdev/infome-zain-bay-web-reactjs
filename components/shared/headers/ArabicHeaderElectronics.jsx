import React, { useEffect } from 'react';

import Link from 'next/link';
import menuData from '~/public/static/data/menu';
import CurrencyDropdown from '~/components/shared/headers/modules/CurrencyDropdown';
import LanguageSwicher from '~/components/shared/headers/modules/LanguageSwicher';
import SearchHeader from '~/components/shared/headers/modules/SearchHeader';
import ElectronicHeaderActions from '~/components/shared/headers/modules/ElectronicHeaderActions';
import Menu from '~/components/elements/menu/Menu';
import { stickyHeader } from '~/utilities/common-helpers';

import constants from '~/public/static/data/my-constants/Constants';
// import labels from '~/public/static/data/my-constants/Labels';
import Image from 'next/image';
import ArabicSearchHeader from './modules/ArabicSearchHeader';
import ArabicElectronicHeaderActions from './modules/ArabicElectronicHeaderActions';
import { Labels } from '~/public/static/data/my-constants/Labels';
// import labelEnglish from '~/public/static/data/my-constants/Labels';

const ArabicHeaderElectronic = ({cartItem,menuItems,language}) => {
    useEffect(() => {
        if (process.browser) {
            window.addEventListener('scroll', stickyHeader);
        }
    }, []);

    const labels = Labels(constants.Arabic)

    return (
        <header
            className="header header--standard header--electronic arabic-header"
            id="headerSticky"
            style={{ width: '100%' }}>
            <div className="header__content"  style={{ width: '100%' }}>
                <div  className="pl-5 pr-5"
                    style={{
                        width: '100%',
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                    <div className="header__content-right" style={{ width: '30%' }}>
                        <ArabicElectronicHeaderActions language={language}/>
                    </div>
                    <div className="header__content-center" style={{ width: '50%' }}>
                        {/* <SearchHeader labels={labels}/> */}
                        <ArabicSearchHeader/>
                    </div>
                    <div className="header__content-left" style={{ width: '20%' }}>
                        <div className="menu--product-categories">
                            <div className="menu__toggle">
                                <i className="icon-menu"
                                    style={{ marginBottom: '1rem' }}></i>
                                <span> {labels['Shop by Department']}</span>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    language={language}
                                    source={menuItems}
                                    className="menu--dropdown"
                                />
                            </div>
                        </div>
                        <Link href="/">
                            <a className="ps-logo">
                                <img
                                    src="/static/img/newLogo.png"
                                    alt="martfury"
                                />
                            </a>
                        </Link>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default ArabicHeaderElectronic;
