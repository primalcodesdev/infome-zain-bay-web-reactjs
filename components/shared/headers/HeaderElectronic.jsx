import React, { useEffect} from 'react';

import Link from 'next/link';
import SearchHeader from '~/components/shared/headers/modules/SearchHeader';
import ElectronicHeaderActions from '~/components/shared/headers/modules/ElectronicHeaderActions';
import Menu from '~/components/elements/menu/Menu';
import { stickyHeader } from '~/utilities/common-helpers';

import labels from '~/public/static/data/my-constants/Labels';
import { useSelector } from 'react-redux';

const HeaderElectronic = ({ language }) => {
    useEffect(() => {
        if (process.browser) {
            window.addEventListener('scroll', stickyHeader);
        }
    }, []);

    const menuItems = useSelector((state) => state.datas.menuItems);


    return (
        <header
            className="header header--standard header--electronic"
            id="headerSticky"
            style={{ width: '100%' }}>
            <div className="header__content" style={{ width: '100%' }}>
                <div
                    className="pl-5 pr-5"
                    style={{
                        width: '100%',
                        display: 'flex',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                    <div
                        className="header__content-left"
                        style={{ width: '20%' }}>
                        <Link href="/">
                            <a className="ps-logo" style={{ marginRight: '0' }}>
                                <img
                                    src="/static/img/newLogo.png"
                                    alt="martfury"
                                />
                            </a>
                        </Link>
                        <div className="menu--product-categories">
                            <div className="menu__toggle">
                                <i
                                    className="icon-menu"
                                    style={{ marginBottom: '1rem' }}></i>
                                <span> {labels['Shop by Department']}</span>
                            </div>
                            <div className="menu__content">
                                <Menu
                                    language={language}
                                    source={menuItems}
                                    className="menu--dropdown"
                                />
                            </div>
                        </div>
                    </div>
                    <div
                        className="header__content-center"
                        style={{ width: '50%' }}>
                        <SearchHeader />
                    </div>
                    <div
                        className="header__content-right"
                        style={{ width: '30%' }}>
                        <ElectronicHeaderActions language={language} />
                    </div>
                </div>
            </div>
        </header>
    );
};

export default HeaderElectronic;
