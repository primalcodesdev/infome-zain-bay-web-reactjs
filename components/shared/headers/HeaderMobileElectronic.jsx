import React, { Component, useState } from 'react';
import CurrencyDropdown from './modules/CurrencyDropdown';
import Link from 'next/link';
import LanguageSwicher from './modules/LanguageSwicher';
import MobileHeaderActions from './modules/MobileHeaderActions';
import constantsEnglish from '~/public/static/data/my-constants/Constants';
// import labelEnglish from '~/public/static/data/my-constants/Labels';
import labels, { Labels } from '~/public/static/data/my-constants/Labels';
import constants from '~/public/static/data/my-constants/Constants';
import { useRouter } from 'next/router';

function HeaderMobileElectronic({language}) {
    // constructor({ props }) {
    //     super(props);
    // }

    // render() {
    const [keyword, setKeyword] = useState('');
    const Router = useRouter()


    function handleSubmit(e) {
        e.preventDefault();
        Router.push(`/shop?search=${keyword}`)

    }

    const labels = Labels(language)

    return (
        <header className="header header--mobile electronic">
           
            <div className="navigation--mobile">
                <div className="navigation__left">
                    <Link href="/">
                        <a className="ps-logo">
                            

                            <img
                                src="/static/img/newLogo.png"
                                alt="martfury"
                                style={{width:"170px",height:"auto"}}
                            />
                        </a>
                    </Link>
                </div>
                <MobileHeaderActions language={language}/>
            </div>
            <div className="ps-search--mobile">
                <form
                    className="ps-form--search-mobile"
                    action="/"
                    method="get"
                    onSubmit={handleSubmit}>
                    <div className="form-group--nest">
                        <input

                            className="form-control"
                            type="text"
                            dir={language=== constants.Arabic && 'rtl'}
                            placeholder={labels["I'm Shopping for..."]}
                            onChange={(e) => setKeyword(e.target.value)}
                        />
                        <button>
                            <i className="icon-magnifier"></i>
                        </button>
                    </div>
                </form>
            </div>
        </header>
    );
    // }
}

export default HeaderMobileElectronic;
