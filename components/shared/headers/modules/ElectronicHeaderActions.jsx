import React, { Component } from 'react';
import { connect, useSelector } from 'react-redux';
import Link from 'next/link';

import MiniCart from './MiniCart';
import AccountQuickLinks from './AccountQuickLinks';
import constants from '~/public/static/data/my-constants/Constants';
import LanguageSwicher from './LanguageSwicher';
import { useRouter } from 'next/router';

function ElectronicHeaderActions({language}) {
    
    const wishlistCount = useSelector((state) => state.datas.wishlistCount);
    const router = useRouter();

    return (
        <div className="header__actions" style={{width:"100%"}}>
            <LanguageSwicher />
            <a
                className="header__extra"
                onClick={() =>
                    router.push(
                        constants['sessionId'] === null
                            ? '/account/login'
                            : '/account/wishlist',
                        null,
                        {
                            shallow: true,
                        }
                    )
                }>
                <i className="icon-heart"></i>
                {(wishlistCount !== null && wishlistCount !== 0) && (
                    <span>
                        <i>{wishlistCount}</i>
                    </span>
                )}
            </a>
            <MiniCart language={language}/>

            {constants['sessionId'] !== null ? (
                <AccountQuickLinks language={language} isLoggedIn={true} />
            ) : (
                <AccountQuickLinks language={language} isLoggedIn={false} />
            )}
        </div>
    );
}

export default ElectronicHeaderActions;
