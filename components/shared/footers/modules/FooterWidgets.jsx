import React from 'react';
import Link from 'next/link';
import { Labels } from '~/public/static/data/my-constants/Labels';
import { useSelector } from 'react-redux';

import constants from '~/public/static/data/my-constants/Constants';
import { useLanguageHook } from '~/components/hooks/useLanguageHook';
import { useWindowSize } from '~/components/partials/homepage/electronic/ElectronicProductGroupWithCarousel';

const FooterWidgets = ({language}) => {
    // const language = useSelector((state) => state.datas.language);
    // const language = useLanguageHook();

    const labels = Labels(language);

    const [width, height] = useWindowSize();


    return (
        <div className="ps-footer__widgets" style={{justifyContent:"normal"}}>
            {language === constants['English'] ? (
                <>
                    <aside className="widget widget_footer widget_contact-us    mb-5" >
                        <h4 className="widget-title">{labels['Contact us']} </h4>
                        <div className="widget_content">
                            <p>{labels['Call us 24/7']}</p>
                            <h3>{labels['+97 4354 6020']}</h3>
                            <p>
                                {labels['office no:102,']} <br />{' '}
                                {labels['Al Rostamani Building,']}
                                <br /> {labels['Khalid Bin Al Waleed Rd,']}
                                {labels['Bur Dubai ,Dubai']} <br />
                                {labels['UAE.United Arab Emirates']}
                                <br />
                                <a href="mailto:contactZainbay@gmail.com">
                                    contactZainbay@gmail.com
                                </a>
                            </p>
                            <ul className="ps-list--social">
                                <li>
                                    <a className="facebook" href="#">
                                        <i className="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a className="twitter" href="#">
                                        <i className="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a className="google-plus" href="#">
                                        <i className="fa fa-google-plus"></i>
                                    </a>
                                </li>
                                <li>
                                    <a className="instagram" href="#">
                                        <i className="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                    <aside className="widget widget_footer" style={{marginLeft:width > 771 && "2rem", }} >
                        <h4 className="widget-title">
                            {' '}
                            {labels['Quick links']}
                        </h4>
                        <ul className="ps-list--link">
                            {/* <li>
                                <Link href="/page/privacy-policy">
                                    <a> {labels['Policy']}</a>
                                </Link>
                            </li> */}

                            <li>
                                <Link href="/page/terms-conditions">
                                    <a>{labels['Term & Condition']}</a>
                                </Link>
                            </li>
                            {/* <li>
                                <Link href="/page/shipping">
                                    <a> {labels['Shipping']}</a>
                                </Link>
                            </li> */}
                            <li>
                                <Link href={`/page/return`}>
                                    <a> {labels['Return']}</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/page/faqs">
                                    <a> {labels['FAQs']}</a>
                                </Link>
                            </li>
                        </ul>
                    </aside>
                    {/* <aside className="widget widget_footer">
                        <h4 className="widget-title">{labels['Company']}</h4>
                        <ul className="ps-list--link">
                            <li>
                                <Link href="/page/about-us">
                                    <a> {labels['About Us']}</a>
                                </Link>
                            </li>

                            <li>
                                <Link href="/page/contact-us">
                                    <a> {labels['Contact']}</a>
                                </Link>
                            </li>
                        </ul>
                    </aside> */}
                </>
            ) : (
                <>
                    {/* <aside className="widget widget_footer" style={{textAlign:"right"}}>
                        <h4 className="widget-title">{labels['Company']}</h4>
                        <ul className="ps-list--link">
                            <li>
                                <Link href="/page/about-us">
                                    <a> {labels['About Us']}</a>
                                </Link>
                            </li>

                            <li>
                                <Link href="/page/contact-us">
                                    <a> {labels['Contact']}</a>
                                </Link>
                            </li>
                        </ul>
                    </aside> */}
                    <aside className="widget widget_footer" style={{textAlign:"right"}}>
                        <h4 className="widget-title">
                            {' '}
                            {labels['Quick links']}
                        </h4>
                        <ul className="ps-list--link">
                            {/* <li>
                                <Link href="/page/blank">
                                    <a> {labels['Policy']}</a>
                                </Link>
                            </li> */}

                            <li>
                                <Link href="/page/blank">
                                    <a>{labels['Term & Condition']}</a>
                                </Link>
                            </li>
                            {/* <li>
                                <Link href="/page/blank">
                                    <a> {labels['Shipping']}</a>
                                </Link>
                            </li> */}
                            <li>
                                <Link href="/page/blank">
                                    <a> {labels['Return']}</a>
                                </Link>
                            </li>
                            <li>
                                <Link href="/page/faqs">
                                    <a> {labels['FAQs']}</a>
                                </Link>
                            </li>
                        </ul>
                    </aside>
                    <aside className="widget widget_footer widget_contact-us mb-5 col-sm-12 col-xs-12 col-md-4" style={{textAlign:"right"}}>
                        <h4 className="widget-title">{labels['Contact us']}  </h4>
                        <div className="widget_content">
                            <p>{labels['Call us 24/7']}</p>
                            <h3>{labels['+97 4354 6020']}</h3>
                            <p>
                                {labels['office no:102,']} <br />{' '}
                                {labels['Al Rostamani Building,']}
                                <br /> {labels['Khalid Bin Al Waleed Rd,']}
                                {labels['Bur Dubai ,Dubai']} <br />
                                {labels['UAE.United Arab Emirates']}
                                <br />
                                <a href="mailto:contactZainbay@gmail.com">
                                    contactZainbay@gmail.com
                                </a>
                            </p>
                            <ul className="ps-list--social">
                                <li>
                                    <a className="facebook" href="#">
                                        <i className="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a className="twitter" href="#">
                                        <i className="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a className="google-plus" href="#">
                                        <i className="fa fa-google-plus"></i>
                                    </a>
                                </li>
                                <li>
                                    <a className="instagram" href="#">
                                        <i className="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                    
                </>
            )}
        </div>
    );
};

export default FooterWidgets;
