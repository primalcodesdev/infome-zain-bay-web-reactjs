import React from 'react';
import ContainerHomeElectronics from '~/components/layouts/ContainerHomeElectronics';

function TermsConditions() {
    return (
        <ContainerHomeElectronics title="terms-conditions" boxed={true}>
            <div className="ps-section--custom">
                <div className="container">
                    <div className="ps-section__header">
                        <h1>Terms&Conditions</h1>
                    </div>
                    <div className="ps-section__content">
                        <h4>1. Introduction</h4>
                        <p>
                            Zainbay.com is owned and managed by Infome
                            Technologies LLC. Infome Technologies LLC is
                            registered in JLT, Dubai, UAE. United Arab of
                            Emirates is our country of domicile.
                        </p>
                        <h4>2. Eligibility for Membership</h4>
                        <p>
                            Minors under the age of 18 shall be prohibited to
                            register as a User of this website and are not
                            allowed to transact or use the website.
                        </p>
                        <h4>3. Online Payments</h4>
                        <p>
                            If you make a payment for our products or services
                            on our website, the details you are asked to submit
                            will be provided directly to our payment provider
                            via a secure connection. We accept payments online
                            using Visa and MasterCard credit/debit cards in AED.
                            The cardholder must retain a copy of transaction
                            records and Merchant policies and rules.
                        </p>
                        <h4>4. Copyright</h4>
                        <p>
                            All content included on the Site, including but not
                            limited to text, graphics, logos, button icons,
                            images, audio clips, digital downloads, data
                            compilations, and software, is the property and
                            copyright work of either ZainBay, its users, its
                            content suppliers or its licensors and is protected
                            by copyright, trademarks, patents or other
                            intellectual property rights and laws. The
                            compilation of the content on the Site is the
                            exclusive property and copyright of ZainBay and is
                            protected by copyright, trademarks, patents, or
                            other intellectual property rights and laws.
                        </p>
                        <h4>5. Governing Law</h4>
                        <p>
                            This User Agreement is governed by and construed in
                            accordance with UAE law. This clause will survive
                            any expiry or cancellation of this User Agreement
                            for any reason.
                        </p>
                        <h4>6. Indemnity</h4>
                        <p>
                            You agree to indemnify and hold ZainBay and its
                            affiliates, officers, employees, agents and
                            suppliers harmless from any and all claims, demands,
                            actions, proceedings, losses, liabilities, damages,
                            costs, expenses (including reasonable legal costs
                            and expenses), howsoever suffered or incurred due to
                            or arising out of shopping, getting a consultation,
                            or visiting our website or any of our physical
                            shops, as well as if you breach this User Agreement,
                            or violate any law or the rights of a third party.
                        </p>
                        <h4>7. Withdrawal of Access and/or Membership</h4>
                        <p>
                            Without prejudice to any other rights and remedies
                            of ZainBay under this User Agreement or at law or
                            otherwise, ZainBay may limit, suspend or withdraw
                            your membership and / or your access to the Site at
                            any time, without notice, for any reason, including
                            without limitation, breach of this User Agreement.
                        </p>
                        <h4>8. Providing Feedback</h4>
                        <p>
                            ZainBay encourages buyers and sellers on the Site to
                            provide feedback on each other&#39;s conduct after a
                            transaction has closed; as this helps all users know
                            what it is like to deal with the said buyer/seller.
                            Your feedback will be displayed along with your user
                            ID on the Site. You cannot retract the feedback once
                            you have left it. ZainBay will not be responsible or
                            liable in any way for the feedback that you post on
                            the Site. You agree not to make comm5profanity,
                            abusing another user, disrespecting another culture
                            or any other derogatory or inappropriate comments.
                            You further agree not to post feedback in order to
                            solicit sales outside of the Site and not to display
                            contact information of any person within any
                            feedback. If you continuously receive negative
                            feedback ratings, without prejudice to any other
                            rights and remedies of ZainBay under this User
                            Agreement or at law, ZainBay reserves the right to
                            suspend, limit or withdraw your access to the Site
                            and/or your membership of the Site. You acknowledge
                            and agree that your feedback consists of comments
                            left by other users with whom you transact and a
                            composite feedback number compiled by ZainBay and
                            that together these convey your full user profile
                        </p>
                        <h4>9. Privacy</h4>
                        <p>
                            ZainBay takes reasonable measures (physical,
                            organizational and technological) to safeguard
                            against unauthorized access to your personally
                            identifiable information and to safely store your
                            personally identifiable information. However, the
                            Internet is not a secure medium and the privacy of
                            your personal information can never be guaranteed.
                            ZainBay has no control over the practices of third
                            parties (e.g. website links to this Site or third
                            parties who misrepresent themselves as you or
                            someone else). You agree that ZainBay may process
                            your personal information that you provide to it for
                            the purposes of providing the services on ZainBay
                            and for sending marketing communications to you and
                            that the Privacy Policy of this Site governs our
                            collection, processing, use, and any transfer of
                            your personally identifiable information.
                        </p>
                        <h4>10. Abusing ZainBay</h4>
                        <p>
                            Please report problems of any kind or violations of
                            this User Agreement to ZainBay. If you believe that
                            your intellectual property rights have been
                            violated, please notify ZainBay. Without prejudice
                            to any other rights and remedies of ZainBay under
                            this User Agreement or at law, ZainBay may limit,
                            suspend or withdraw a user&#39;s access to the Site
                            and/or a user&#39;s membership of the Site or remove
                            hosted content. Also ZainBay can choose to take
                            other technical and/or legal steps against users who
                            create problems or possible legal liabilities of any
                            kind, who infringe the intellectual property rights
                            of third parties or who act inconsistently with this
                            User Agreement or our policies.
                        </p>
                        <h4>11. Trademarks</h4>
                        <p>
                            &quot;ZainBay&quot; and related logos, and other
                            words and logos on the Site are either unregistered
                            trademarks or registered trademarks of ZainBay and
                            are protected by international trademark and other
                            intellectual property rights and laws. ZainBay&#39;s
                            trademarks may not be used in connection with any
                            product or service that is not ZainBay&#39;s nor in
                            any manner that disparages or discredits ZainBay.
                            All other trademarks not owned by ZainBay that
                            appear on the Site are the property of their
                            respective owners, who may or may not be affiliated
                            with, connected to, or sponsored by ZainBay.
                        </p>
                        <h4>12. Disputes</h4>
                        <p>
                            Any dispute or claim arising out of or in connection
                            with this website shall be governed and construed in
                            accordance with the laws of UAE.
                        </p>
                    </div>
                </div>
            </div>
        </ContainerHomeElectronics>
    );
}

export default TermsConditions;
