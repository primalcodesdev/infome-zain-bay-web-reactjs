import React from 'react';
import BreadCrumb from '~/components/elements/BreadCrumb';
import ContainerHomeElectronics from '~/components/layouts/ContainerHomeElectronics';

function Return() {
    const breadCrumb = [
        {
            text: 'Home',
            url: '/',
        },
        {
            text: 'privacy policy',
        },
    ];
    return (
        <ContainerHomeElectronics title="Orderd" boxed={true}>
            <div className="ps-page--my-account">
                <BreadCrumb breacrumb={breadCrumb} />
                <div className="ps-section--custom">
                    <div className="container">
                        <div className="ps-section__header">
                            <h2>Return Policy</h2>
                        </div>
                        <div className="ps-section__content">
                            <p>
                                Please read all the conditions below. Zainbay
                                reserves the right to refuse a return / refund /
                                exchange request. Please make your claims for
                                "damaged items on delivery" or missing items
                                within 12 hours of receipt of your order.
                            </p>
                            <h3>Is my product eligible for return?</h3>
                            <p>
                                All of the products sold on Zainbay are eligible
                                for returns, if not stated otherwise on the
                                product page. Some products may not be eligible
                                for returns due to the nature of the product.
                                These may include assembled PCs (as the
                                components are unboxed to be used in the
                                computer assembly). If you decide to return your
                                purchase, you can do so within 14 days after
                                your purchase. The following conditions apply:
                            </p>
                            <ul
                                style={{
                                    listStyleType: 'none',
                                    color: '#676767',
                                }}>
                                <li>
                                    1- The product must be returned with the
                                    original packaging, sealed, and unopened and
                                    must include all packing material, blank
                                    warranty cards, manuals, and all
                                    accessories.
                                </li>
                                <li>
                                    2- If the product has been used, or the
                                    product’s original box was opened, or the
                                    original seal was removed, we will decline
                                    the refund and return the product back to
                                    you unless there is a manufacturing defect
                                </li>
                                <li>
                                    3- If the product is not working due to a
                                    manufacturing defect, the original box,
                                    information booklet, and all other
                                    accessories must be returned too
                                </li>
                            </ul>
                            <p>
                                Goods must be returned to our Returns Department
                                either by yourself, or sent by a
                                courier.Delivery charges for returning an
                                item(s) are to be paid and are the
                                responsibility of the customer. Refunds will be
                                done only after we receive & verify the items.
                                Include the invoice with the return. If the
                                goods are defective, please specify the defect,
                                in a separate sheet of paper and attach it along
                                with the invoice. Please DO NOT write or mark
                                anywhere on the product's factory packing
                                (carton) or product itself.
                            </p>

                            <p>
                                Return or exchange of the products is only
                                possible within the fourteen days of the date of
                                delivery according to the terms given herein.
                                All requests for exchange and returns must be
                                accompanied by the original invoice or the
                                warranty card.
                            </p>

                            <p>
                                If your item is eligible for a return, you will
                                be able to raise the return request through the
                                Orders Section in your Zainbay user account. The
                                Zainbay returns team shall contact you
                            </p>
                            <p>
                                within 24 hours for claim validation. After
                                confirmation, we will send a courier to collect
                                the item to be returned from you. Please make
                                your claims for "damaged items on delivery" or
                                missing items within 6 hours of receipt of
                                merchandise.
                            </p>

                            <p>
                                Exchange or Returns shall not be applicable to
                                such products falling to the categories as;
                            </p>

                            <ul style={{ color: '#676767' }}>
                                <li>Accessories</li>
                                <li>Software products</li>
                                <li>Cartridges</li>
                                <li>Toners</li>
                                <li>
                                    Any customized configuration product
                                    supplied
                                </li>
                                <li>Any non-local Distributors products</li>
                                <li>Special Order</li>
                            </ul>
                            <p>
                                Please note that you should be extremely careful
                                while receiving and installing the below
                                category products. Our team is doing complete
                                safety checkup before sending the shipment &
                                will not provide any compensation in case of any
                                physical damage occurring from the customer.
                            </p>
                            <p>
                                If damage is occurred during the shipment
                                process, the customer should immediately contact
                                us when the product is received.
                            </p>
                            <h4>Returns.</h4>
                            <p>
                                All returns will be charged at a minimum of 20%
                                of the product value irrespective of the
                                condition of the product. Delivery and shipping
                                charges are not refundable at the moment and are
                                borne by the customer.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </ContainerHomeElectronics>
    );
}

export default Return;
