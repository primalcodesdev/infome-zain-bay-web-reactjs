import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import SkeletonProductDetail from '~/components/elements/skeletons/SkeletonProductDetail';
import BreadCrumb from '~/components/elements/BreadCrumb';
import ProductDetailFullwidth from '~/components/elements/detail/ProductDetailFullwidth';
import HeaderProduct from '~/components/shared/headers/HeaderProduct';
import HeaderDefault from '~/components/shared/headers/HeaderDefault';
import ContainerHomeElectronics from '~/components/layouts/ContainerHomeElectronics';
import apis from '~/public/static/data/my-constants/Api';
import Axios from 'axios';
import { ShareDataContext } from '~/utilities/share-data-context';
import ArabicProductDetailFullwidth from '~/components/elements/detail/ArabicProductDetailFullwidth';
import constants from '~/public/static/data/my-constants/Constants';
import { useDispatch, useSelector } from 'react-redux';
import { setProVarient } from '~/store/datas/action';
import ElectronicProductGroupWithCarousel from '~/components/partials/homepage/electronic/ElectronicProductGroupWithCarousel';
import RecentlyViewedProduct from '~/components/partials/product/RecentlyViewedProduct';
import Error from '../404';


const ProductDefaultPage = () => {
    const dispatch = useDispatch();
    const router = useRouter();
    const { pid } = router.query;
    const [product, setProduct] = useState(null);
    const [apiSuccess, setApiSuccess] = useState(false);
    const [apiError, SetApiError] = useState(false);
    const [productVarient, setProductVarient] = useState(null);
    const [productImage, setProductImage] = useState(null);
    const [productDescription, setProductDescription] = useState('');
    const [metaTagKeywords, setMetaTagKeywords] = useState('');



    const [lang, setLang] = useState(false);
    const changeLanguage = useSelector((state) => state.datas.changeLanguage);
    useEffect(() => {
        setLang(
            localStorage.getItem('language') === null
                ? 'english'
                : localStorage.getItem('language')
        );
    }, []);

    useEffect(() => {
        setLang(
            localStorage.getItem('language') === null
                ? 'english'
                : localStorage.getItem('language')
        );
    }, [changeLanguage]);

    const prVarientId = useSelector((state) => state.datas.proVarient);

    async function getProduct(pid) {
        Axios.post(apis.productDetail, {
            session_id: constants['sessionId'],
            language: lang,
            slug_Id: pid,
        })
            .then((res) => {
                setProductDescription(res.data.data.product[0].Description);
                setProductImage(res.data.data.product[0].Thumbnail_images);

                setProduct([res.data.data.product[0]]);
                setApiSuccess(true);
            })
            .catch((err) => {
                SetApiError(true);
            });
    }

    const { asPath } = useRouter();

    const metaTagDatas = () => {
        Axios.post(apis.metaTags, {
            url: asPath,
        })
            .then((res) => {

                setMetaTagKeywords(res.data.data.meta_tags);
            })
            .catch((err) => {
               
            });
    };

    useEffect(() => {
        metaTagDatas();
        lang !== false && getProduct(pid);
        if (productVarient === null) {
            setProductVarient(pid);
        }
        if (prVarientId === null) {
            dispatch(setProVarient(pid));
            setProductVarient(pid);
        }
    }, [pid,lang]);

    const breadCrumb = [
        {
            text: 'Home',
            url: '/',
        },
        {
            text: 'Shop',
            url: '/shop',
        },
        {
            text: product ? product.title : 'Loading...',
        },
    ];

    let productView, relatedProducts,headerView, recentlyViewedProducts;
    if (apiSuccess) {
        if (product) {
            if (lang === constants['Arabic']) {
                productView = (
                    <ArabicProductDetailFullwidth language={lang} product={product} />
                );
            } else {
                productView = <ProductDetailFullwidth language={lang} product={product} />;
            }
            product[0].related_products.length !== 0
                ? (relatedProducts = (
                      <ElectronicProductGroupWithCarousel
                            // index={i}

                            relatedProducts={true}

                          language={lang}
                          Slug={null}
                          title={'Related Products'}
                          products={product[0].related_products}
                          arabicTitle={'المنتجات المعروضة مؤخرا'}
                          type={'noViewAll'}
                      />
                  ))
                : (relatedProducts = null);

            product[0].recently_viewed_products.length !== 0
                ? (recentlyViewedProducts = (
                      <RecentlyViewedProduct
                          products={product[0].recently_viewed_products}
                      />
                  ))
                : (recentlyViewedProducts = null);

            headerView = <HeaderProduct product={product} />;
        } else {
            headerView = <HeaderDefault />;
        }
    } else if (apiError) {
        productView = <Error />;
    } else {
        productView = <SkeletonProductDetail />;
    }

    return (
        <>
            {lang !== false && (
                <ShareDataContext.Provider
                    value={{
                        product_varient: [productVarient, setProductVarient],
                    }}>
                    <ContainerHomeElectronics
                        boxed={true}
                        title={pid.replace(/-/g, ' ')}
                        navigationList={`productDetail`}
                        metaTags={`${pid.replace(
                            /-/g,
                            ' '
                        )},${metaTagKeywords.toString()}`}
                        metaDescription={productDescription.replace(
                            /<[^>]*>?/gm,
                            ''
                        )}>
                        <BreadCrumb
                            breacrumb={breadCrumb}
                            products={product}
                            layout="fullwidth"
                        />
                        <div className="ps-page--product">
                            <div className="ps-container">
                                <div className="ps-page__container">
                                    {productView}
                                </div>
                                {relatedProducts}
                                {recentlyViewedProducts}
                            </div>
                        </div>
                    </ContainerHomeElectronics>
                </ShareDataContext.Provider>
            )}
        </>
    );
};

export default ProductDefaultPage;
